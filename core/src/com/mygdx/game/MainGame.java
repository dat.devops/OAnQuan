package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.screens.ScreenManager;
import com.mygdx.game.screens.WelcomeScreen;

/**
 * Created by kienvanba on 5/8/17.
 */

public class MainGame extends ApplicationAdapter {
    private SpriteBatch batch;
    public static Music music;

    @Override
    public void create() {
        batch = new SpriteBatch();
        music = Gdx.audio.newMusic(Gdx.files.internal("in_game_music.mp3"));
        music.setLooping(true);
        music.setVolume(0.3f);
        music.play();
        ScreenManager.getInstance().push(new WelcomeScreen());
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        ScreenManager.getInstance().update(Gdx.graphics.getDeltaTime());
        ScreenManager.getInstance().render(batch);
    }

    @Override
    public void dispose() {
        super.dispose();
        music.dispose();
    }
}
