package com.mygdx.game.models;

import com.badlogic.gdx.math.Rectangle;

/**
 * Created by d40 on 5/9/17.
 */

public class Arrow {
    private Rectangle triangle;
    private boolean direction;

    public static final class Direction {
        public static final boolean LEFT = false;
        public static final boolean RIGHT = true;
    }

    public Arrow(Rectangle rect, boolean direction){
        triangle = rect;
        this.direction = direction;
    }

    public Rectangle getTriangle() {
        return triangle;
    }

    public boolean getDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }
}
