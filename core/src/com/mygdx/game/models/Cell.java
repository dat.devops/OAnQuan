package com.mygdx.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by kienvanba on 5/8/17.
 */

public class Cell {
    private Rectangle area;
    private boolean hasKing;
    private boolean isSelected;

    private ArrayList<Rock> rocks;

    public Cell(Rectangle rect, boolean hasKing) {
        this.area = rect;
        this.hasKing = hasKing;
        initRocks();
    }

    private void initRocks(){
        this.rocks = new ArrayList<Rock>();
        if(hasKing) {
            rocks.add(new Rock(new Vector3(area.getX() + 10, area.getY() + 35, 0), true));
        }else {
            for (int i = 0; i < 5; i++) {
                rocks.add(new Rock(this.getRandPosition(), false));
            }
        }
    }

    public Rectangle getArea(){
        return this.area;
    }

    public Vector3 getRandPosition(){
        Random random = new Random();
        int x = random.nextInt((int)area.getWidth()-25)+10;
        int y = random.nextInt((int)area.getHeight()-25)+10;
        return new Vector3(area.getX()+x,area.getY()+y,0);
    }

    public boolean contains(float x, float y){
        Rectangle rectangle = new Rectangle(
                area.getX()+10, area.getY()+10,
                area.getWidth()-25, area.getHeight()-25
        );
        return rectangle.contains(x,y);
    }

    public Rectangle getRandArea(){
        Random random = new Random();
        float posX = area.getX();
        float posY = area.getY();
        return new Rectangle(
                random.nextInt((int)area.getWidth()-20)+posX,
                random.nextInt((int)area.getHeight()-20)+posY,
                25,
                25
        );
    }

    public void changePosition(Vector3 newPosition){
        this.area.setPosition(newPosition.x, newPosition.y);
    }

    public ArrayList<Rock> getRocks(){
        return rocks;
    }
    public void setRocks(ArrayList<Rock> list){
        this.rocks = list;
    }
    public boolean isSelected(){
        return this.isSelected;
    }
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public void select(){
        this.isSelected=true;
    }
    public void release(){
        this.isSelected=false;
    }
    public boolean hasKing(){
        for(Rock rock: rocks){
            if(rock.isKing())
                return true;
        }
        return false;
    }
    public boolean isOn(float x, float y){
        return this.area.contains(x,y);
    }
    public int getScore(){
        int score=0;
        for(Rock rock: rocks){
            if(rock.isKing()){
                score+=5;
            }else{
                score++;
            }
        }
        return score;
    }

    public void dispose() {
        for(Rock rock: rocks){
            rock.dispose();
        }
    }
}
