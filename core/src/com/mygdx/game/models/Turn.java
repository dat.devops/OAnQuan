package com.mygdx.game.models;

/**
 * Created by d40 on 5/9/17.
 */

public class Turn {
    private static final Turn ourInstance = new Turn();
    private boolean turn;

    public static final boolean PLAYERONE = false;
    public static final boolean PLAYERTWO = true;

    public static Turn getInstance() {
        return ourInstance;
    }

    public boolean getTurn(){
        return turn;
    }

    public void switchTurn(){
        turn = !turn;
    }

    private Turn() {
        turn = Turn.PLAYERONE;
    }

    private boolean winner;
    public void setWinner(boolean winner){
        this.winner = winner;
    }
    public boolean getWinner(){
        return this.winner;
    }
}
