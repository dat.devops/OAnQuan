package com.mygdx.game.screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by kienvanba on 5/8/17.
 */

public abstract class Screen {
    protected AssetManager mAssets;
    protected OrthographicCamera camera;
    public Screen(){
        camera = new OrthographicCamera();
        mAssets = ScreenManager.getInstance().getAssetManager();
    }

    protected abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch batch);
    public abstract void dispose();
}
