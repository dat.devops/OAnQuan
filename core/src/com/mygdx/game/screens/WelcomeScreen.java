package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.Constants;
import com.mygdx.game.models.Animation;

/**
 * Created by kienvanba on 5/8/17.
 */

public class WelcomeScreen extends Screen {
    private Texture background;
    private ShapeRenderer shapeRenderer;
    private Texture playLine;
    private float progress;
    private boolean showProgress;
    public WelcomeScreen(){
        camera.setToOrtho(false, Constants.SCREEN_WIDTH/2, Constants.SCREEN_HEIGHT/2);
        background = new Texture("white_paper_bg.jpg");
        playLine = new Texture("any.png");
        shapeRenderer = new ShapeRenderer();
        progress = 0f;

        debug();
    }
    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()){
            startLoadingAsset();
        }
    }

    private void debug(){
        System.out.println("camera position: "+camera.position.toString());
        System.out.println("camera viewPort w: "+camera.viewportWidth);
        System.out.println("camera viewPort h: "+camera.viewportHeight);
    }

    private void startLoadingAsset(){
        showProgress = true;
        mAssets.load("red_ball.png", Texture.class);
        mAssets.load("green_ball.png", Texture.class);
        mAssets.load("Board.png", Texture.class);
        mAssets.load("player1_win.png", Texture.class);
        mAssets.load("player2_win.png", Texture.class);

        mAssets.load("zero.png", Texture.class);
        mAssets.load("zero_invert.png", Texture.class);
        mAssets.load("one.png", Texture.class);
        mAssets.load("one_invert.png", Texture.class);
        mAssets.load("two.png", Texture.class);
        mAssets.load("two_invert.png", Texture.class);
        mAssets.load("three.png", Texture.class);
        mAssets.load("three_invert.png", Texture.class);
        mAssets.load("four.png", Texture.class);
        mAssets.load("four_invert.png", Texture.class);
        mAssets.load("five.png", Texture.class);
        mAssets.load("five_invert.png", Texture.class);
        mAssets.load("six.png", Texture.class);
        mAssets.load("six_invert.png", Texture.class);
        mAssets.load("seven.png", Texture.class);
        mAssets.load("seven_invert.png", Texture.class);
        mAssets.load("eight.png", Texture.class);
        mAssets.load("eight_invert.png", Texture.class);
        mAssets.load("nine.png", Texture.class);
        mAssets.load("nine_invert.png", Texture.class);

        mAssets.load("mute.png", Texture.class);
        mAssets.load("speaker.png", Texture.class);
        mAssets.load("exit-door.png", Texture.class);

        mAssets.load("game_bg.jpg", Texture.class);
        mAssets.load("white_paper_bg.jpg", Texture.class);
        mAssets.load("line_paper_bg.jpg", Texture.class);
        mAssets.load("line_paper_bg2.jpg", Texture.class);
        mAssets.load("sky.JPG", Texture.class);
        mAssets.load("sound1.ogg", Sound.class);
        mAssets.load("sound2.ogg", Sound.class);
    }

    @Override
    public void update(float dt) {
//        beginLineAnimation.update(dt);
        handleInput();
//        progress = MathUtils.lerp(progress, mAssets.getProgress(), .1f);
        progress = mAssets.getProgress();
        if(showProgress && mAssets.update()){
            ScreenManager.getInstance().push(new GameScreen());
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(
                background,
                0,
                0,
                Constants.SCREEN_WIDTH,
                Constants.SCREEN_HEIGHT
        );
        batch.draw(
                playLine,
                camera.position.x - (playLine.getWidth()/8),
                camera.position.y,
                playLine.getWidth()/4,
                playLine.getHeight()/4
        );
        batch.end();
        if(showProgress){
            shapeRenderer.setProjectionMatrix(camera.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(Color.BLACK);
            shapeRenderer.rect(
                    camera.position.x-(camera.viewportWidth/4),
                    camera.position.y-(camera.viewportHeight/3),
                    camera.viewportWidth/2,
                    10
            );
            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.rect(
                    camera.position.x - (camera.viewportWidth/4)+1,
                    camera.position.y - (camera.viewportHeight/3)+1,
                    (camera.viewportWidth/2-2)*progress,
                    8
            );
            shapeRenderer.end();
        }
    }

    @Override
    public void dispose() {
        background.dispose();
        playLine.dispose();
        shapeRenderer.dispose();
    }
}
